// Import a library to help create a component
import React from 'react';
import { View, AppRegistry, NavigatorIOS } from 'react-native';
import ProductList from './src/components/ProductList';


// Create a component
const App = () => {
    return (
    <NavigatorIOS
        initialRoute={{
          component: ProductList,
          title: 'Aston Villa',
          passProps: {index: 1},
        }}
        style={{flex: 1}}
      />
    );
};



// Render it to the device
AppRegistry.registerComponent('shopar', () => App);
