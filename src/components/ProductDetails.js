import React, { Component } from 'react';
import { ScrollView, Text , View } from 'react-native';
import axios from 'axios';
import PropTypes from 'prop-types';

class ProductDetails extends Component {

    state = {data: {}};
    static propTypes = {
        route: PropTypes.shape({
          title: PropTypes.string.isRequired,
        }),
        navigator: PropTypes.object.isRequired,
      };

      constructor(props, context) {
        super(props, context);
       
      }

      componentWillMount()
      {
          axios.get(this.props.link).then(response => this.setState({data: response.data}));
      }

      renderDetails()
      {
          const {name, brand, long_description} = this.state.data;
          return (
              <View>
                  <Text>{name}</Text>
                  <Text>{brand}</Text>
                  <Text>{long_description}</Text>
               </View>
             
          );
      }
    render() {
        console.log('this.state: ' +this.state);
        return (
            <ScrollView>
                {this.renderDetails()}
            </ScrollView>
        );
    }
};

export default ProductDetails;
