import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';
import Cell from './Cell';
import CellSection from './CellSection';

const ProductCell = ({ product, onPress }) => {

    const { textContainerStyle, headerTextStyle, thumbnailStyle, thumbnailContainerStyle } = styles;
    return (
        <TouchableOpacity onPress={onPress}>
            <Cell>
                <CellSection>
                    <View style={thumbnailContainerStyle}>
                        <Image 
                            style={thumbnailStyle}
                            source={{ uri: product.image.link }} 
                        />
                    </View>
                    <View style={textContainerStyle}>
                        <Text style={headerTextStyle}>{product.product_name}</Text>
                        <Text>&euro;{product.price}</Text>
                    </View>
                </CellSection>
            </Cell>
        </TouchableOpacity>
    );
};

const styles = {
    textContainerStyle: {
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    headerTextStyle: {
        fontSize: 14,
        fontWeight: 'bold',
    },
    thumbnailStyle: {
        height: 50,
        width: 50
    },
    thumbnailContainerStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        marginRight: 10
    }
};

export default ProductCell;
