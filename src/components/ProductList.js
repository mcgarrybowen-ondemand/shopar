import React, { Component } from 'react';
import { ScrollView, Linking } from 'react-native';
import axios from 'axios';
import PropTypes from 'prop-types';
import ProductCell from './ProductCell';
import ProductDetails from './ProductDetails';


class ProductList extends Component {


    static propTypes = {
        route: PropTypes.shape({
          title: PropTypes.string.isRequired,
        }),
        navigator: PropTypes.object.isRequired,
      };

    state = { products: [] };

    constructor(props, context) {
        super(props, context);
        this._onItemPress = this._onItemPress.bind(this);
      }


    componentWillMount() {
        axios.get('https://dev45-web-ecommera.demandware.net/s/SiteGenesisGlobal/dw/shop/v16_1/product_search?q=aston&expand=availability,images,prices&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa').then(response => this.setState({ products: response.data.hits }));
    }

    _onItemPress (product) {
        console.log("onItemPress: " +product.link);
        let nextIndex = ++ this.props.index;
        this.props.navigator.push({
            component: ProductDetails,
            title: product.product_name,
            passProps: {link: product.link}
        })
       
      
    }
 
    renderProducts() {
        return this.state.products.map(product => 
            <ProductCell 
                key={product.product_id} 
                product={product} 
                onPress={() => this._onItemPress(product)}
            />
        );
    }

    render() {
        console.log(this.state);
        return (         
            <ScrollView>
                {this.renderProducts()}
            </ScrollView>
        );
    }
   
}


export default ProductList;